# Yahtzee

**Desc: Implementation of Yahtzee for HW1 in Agile Software Development course**
## Roll your dice!



###n of a kind
    1. sort the list
    2. state: {prev, counter, score}
       initial state: {-1, 1, 0}
    3. using reduce() over list to find if it has n of a kind elements

###full house
    1. sort the list
    2. two cases -> solved by pattern matching dices

###n consequtive
	1. sort the list
    2. make the list distinct (unique)
    3. state: {prev, counter, score}
       initial state:  {-1, 1, 0}
    4. using reduce() over list to find if it has n consequtive elements

###yahtzee
    1. one case -> solved by pattern matching dices



