defmodule Yahtzee do
  defp mapper(n, dice) do
    length(Enum.filter(dice, fn e -> e == n end))
  end

  def score_upper(dice) do
    faces = [:Ones, :Twos, :Threes, :Fours, :Fives, :Sixes]
    Enum.zip(faces, 1..6) |> Enum.into(%{}, fn {face, n} -> {face, mapper(n, dice)} end)
  end

  defp n_of_a_kind_from_sorted(dices, n, x, {prev, counter, score}) do
    cond do
      (x == prev) && (counter == n - 1) -> {x, counter + 1, Enum.sum dices}
      (x == prev) -> {x, counter + 1, score}
      true -> {x, 1, score}
    end
  end

  defp n_consequtive_from_sorted_unique(n, x, max_score, {prev, counter, score}) do
    cond do
      (x == prev + 1) && (counter == n - 1) -> {x, counter, max_score}
      (x == prev + 1) -> {x, counter + 1, score}
      true -> {x, 1, score}
    end
  end

  defp n_of_kind_sorted(dices, n) do
    {_, _, score} = Enum.reduce(dices, {-1, 1, 0}, fn(x, state) -> n_of_a_kind_from_sorted(dices, n, x, state) end)
    score
  end

  defp n_consequtive_sorted(dices, max, n) do
    {_, _, score} = Enum.reduce(dices, {-1, 1, 0}, fn(x, state) -> n_consequtive_from_sorted_unique(n, x, max, state) end)
    score
  end

  defp full_house_sorted([x, x, x, y, y]), do: 25
  defp full_house_sorted([x, x, y, y, y]), do: 25
  defp full_house_sorted(_), do: 0

  defp yahtzee([x, x, x, x, x]), do: 50
  defp yahtzee(_), do: 0

  def score_lower(dice) do
    sorted = dice |> Enum.sort
    %{
      "Three of a kind": n_of_kind_sorted(sorted, 3),
      "Four of a kind":  n_of_kind_sorted(sorted, 4),
      "Full house": full_house_sorted(sorted),
      "Small straight": n_consequtive_sorted(sorted |> Enum.uniq, 30, 4),
      "Large straight": n_consequtive_sorted(sorted |> Enum.uniq, 40, 5),
      "Yahtzee": yahtzee(sorted),
      "Chance": Enum.sum dice
    }
  end
end
